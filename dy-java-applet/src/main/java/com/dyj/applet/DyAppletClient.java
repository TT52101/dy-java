package com.dyj.applet;

import com.dtflys.forest.annotation.BaseRequest;
import com.dyj.common.client.BaseClient;

/**
 * @author danmo
 * @date 2024-04-16 15:28
 **/
@BaseRequest(baseURL = "${domain}")
public class DyAppletClient extends BaseClient {


}
